import React from 'react';
import logo from './logo.svg';
import './App.css';


//TUGAS 2 - KOMPONEN
function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Selamat datang di aplikasi React
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}


// function App() {
//   return (
//     <div>
//       <h1>APP2 Hello Word!!</h1>
//       <h2>Test</h2>
//       </div>
//   );
// }
// function App2() {
//   return (
//     <div>
//       <h1>APP2 Hello Word!!</h1>
//       <h2>Test</h2>
//     </div>
//   );
// }


export default App;
