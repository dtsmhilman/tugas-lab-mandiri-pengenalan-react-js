import React from 'react'
import logo from '../../logo.svg'
import './Fungsi2.css'

//TUGAS 2 - KOMPONEN
function Fungsi2(){
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h3>Selamat Datang di Aplikasi React</h3>
            <form>
                <div className="container">
                    <input type="text" placeholder="Masukan Username" name="uname" required/><br/>
                    <input type="password" placeholder="Masukan Password" name="psw" required/><br/>
                    <button type="submit">Login</button><br/>
                </div>
            </form>
            </header>
        </div>
    )
}

export default Fungsi2;