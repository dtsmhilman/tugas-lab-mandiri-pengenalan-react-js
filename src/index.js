import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
import Fungsi2 from './component/Fungsi2/Fungsi2'
import * as serviceWorker from './serviceWorker';
import './component/Fungsi/Fungsi';
// import Fungsi from './component/Fungsi/Fungsi';
// import NavBar from './component/NavBar/NavBar'
// import gambar from './assets/image/logo.svg'
// import Kelas from './component/Kelas/Kelas'
// import Image from './assets/image/logo.svg';


// TUGAS 2 - KOMPONEN
ReactDOM.render(
  <React.StrictMode>
    <Fungsi2 />
  </React.StrictMode>,
  document.getElementById('root')
);

//TUGAS 1 JSX
// const nama = 'Muhmad Hilman'
// const tahun = 2020
// const lahir = 1997
// const element_1 = (
//       <div>
//           <div className="container">
//               <h3 className="warna">Nama lengkap saya adalah {nama}, Usia {tahun-lahir} pada tahun {tahun}.
//               </h3>
//           </div>
//       </div>
// )

// ReactDOM.render(
//   element_1,
//   document.getElementById('root')
// );

// const element_2 = React.createElement('h3', {className:'warna'}, 'Selamat Pagi')

// const nama = 'Muhmad Hilman'


// const tahun = 2020
// const lahir = 1997
// const bio_1 =(
//   tahun-lahir
// )
// const bio = React.createElement('h3', {className:'container'} + {className:'card'}, `Muhamad Hilman pada ${tahun} tepat Usia ${bio_1}` )



// ReactDOM.render(
//   bio,
//   document.getElementById('coba')
// );


// ReactDOM.render(
//   bio_1,
//   document.getElementById('tgs1')
// );
// ReactDOM.render(
//   <React.StrictMode>
//     <NavBar />
//     <Kelas gambar = {gambar} />
//     <Fungsi fungsi1='Fungsi Material'
//     fungsi2='Fungsi C'
//     fungsi3='Fungsi D'/>
//   </React.StrictMode>,
//   document.getElementById('root')
// );


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
